<?php

/**
 * @file
 * Class definition of FeedsOgMembershipProcessor.
 */

/**
 * Creates nodes from feed items.
 */
class FeedsOgMembershipProcessor extends FeedsProcessor {

  /**
   * Define entity type.
   */
  public function entityType() {
    return 'og_membership';
  }

  /**
   * Implements parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label plural'] = t('OG memberships');
    return $info;
  }

  /**
   * Creates a new og_membership in memory and returns it.
   */
  protected function newEntity(FeedsSource $source) {
    $og_membership = og_membership_create($this->config['group_type'], 0, $this->config['entity_type'], 0, $this->config['field_name']);
    return $og_membership;
  }

  /**
   * Check that the user has permission to save a node.
   */
  protected function entitySaveAccess($og_membership) {
    if ($this->config['authorize']) {
      // @todo: Make sure the current user has permission to add memberships to
      // the given OG group.
    }
  }

  /**
   * Save a node.
   */
  public function entitySave($og_membership) {
    og_membership_save($og_membership);
  }

  /**
   * Delete a series of nodes.
   */
  protected function entityDeleteMultiple($ids) {
    og_membership_delete_multiple($ids);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'group_type' => 'node',
      'entity_type' => 'user',
      'field_name' => 'og_user_node',
      'authorize' => TRUE,
    ) + parent::configDefaults();
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);

    $form['group_type'] = array(
      '#type' => 'select',
      '#title' => t('Group type'),
      // @todo: Allow use of other group types.
      '#options' => array(
        'node' => t('Node'),
      ),
      '#description' => t('The entity type used for groups.'),
      '#default_value' => $this->config['group_type'],
    );

    $form['entity_type'] = array(
      '#type' => 'select',
      '#title' => t('Entity type'),
      // @todo: Allow use of other entity types.
      '#options' => array(
        'user' => t('User'),
      ),
      '#description' => t('The entity type that we are adding to the groups.'),
      '#default_value' => $this->config['group_type'],
    );

    $form['field_name'] = array(
      // @todo: This should be a select of only valid options.
      '#type' => 'textfield',
      '#title' => t('Field name'),
      '#description' => t('The name of the field used to associate the group with the entity.'),
      '#default_value' => $this->config['field_name'],
    );

    $form['authorize'] = array(
      '#type' => 'checkbox',
      '#title' => t('Authorize'),
      '#description' => t('Check that the user has permission to create the memberships.'),
      '#default_value' => $this->config['authorize'],
    );

    return $form;
  }

  /**
   * Find an entity by its label.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $label
   *   The entity label to finde.
   *
   * @return int|NULL
   *   The entity id of the entity if found; NULL otherwise.
   */
  protected function findEntityByLabel($entity_type, $label) {
    $entity_info = entity_get_info($entity_type);

    // Force the user entity label to be 'name'.
    if ($entity_type == 'user' && !isset($entity_info['entity keys']['label'])) {
      $entity_info['entity keys']['label'] = 'name';
    }
    
    // We can't find anything if the entity doesn't have a label.
    if (isset($entity_info['entity keys']['label'])) {
      $base_table = $entity_info['base table'];
      $query = db_select($base_table)
        ->fields($base_table, array($entity_info['entity keys']['id']))
        ->condition($base_table . '.' . $entity_info['entity keys']['label'], $label)
        ->orderBy($base_table . '.' . $entity_info['entity keys']['label'], 'ASC')
        ->addTag($entity_type . '_access');

      $ids = $query->execute()->fetchCol();
      if (count($ids)) {
        return $ids[0];
      }
    }

    throw new Exception(t('Unable to find %entity_type with label %label', array(
      '%entity_type' => $entity_info['label'],
      '%label' => $label,
    )));
  }

  /**
   * Convert targets into real entity properties.
   */
  protected function convertTargets($targets) {
    $result = array();
    foreach ($targets as $name => $value) {
      switch ($name) {
        case 'gid_label':
          $result['gid'] = $this->findEntityByLabel($this->config['group_type'], $value);
          break;

        case 'etid_label':
          $result['etid'] = $this->findEntityByLabel($this->config['entity_type'], $value);
          break;

        case 'user_mail':
          if ($account = user_load_by_mail($value)) {
            $result['etid'] = $account->uid;
          }
          else {
            throw new Exception(t('Unable to find %entity_type with e-mail %mail', array(
              '%entity_type' => t('User'),
              '%mail' => $value,
            )));
          }
          break;

        default:
          $result[$name] = $value;
      }
    }
    return $result;
  }

  /**
   * Override setTargetElement to operate on a target item that is a node.
   */
  public function setTargetElement(FeedsSource $source, $target_entity, $target_element, $value) {
    foreach ($this->convertTargets(array($target_element => $value)) as $target_element => $value) {
      parent::setTargetElement($source, $target_entity, $target_element, $value);
    }
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();

    $targets['gid_label'] = array(
      'name' => t('Group (label)'),
      'description' => t('The name of the group to add the membership to.'),
    );
    $targets['etid_label'] = array(
      'name' => t('Entity (label)'),
      'description' => t('The entity (usually user) to add to the group.'),
    );

    if ($this->config['entity_type'] == 'user') {
      $targets['user_mail'] = array(
        'name' => t('Entity (user e-mail)'),
        'description' => t('The e-mail of the user entity to add to the group.'),
      );
    }

    // Let other modules expose mapping targets.
    self::loadMappers();
    $entity_type = $this->entityType();
    $bundle = $this->bundle();
    drupal_alter('feeds_processor_targets', $targets, $entity_type, $bundle);

    return $targets;
  }

  /**
   * Implements FeedsProcessor::existingEntityId().
   */
  protected function existingEntityId(FeedsSource $source, FeedsParserResult $result) {
    if ($id = parent::existingEntityId($source, $result)) {
      return $id;
    }

    // Use the parser to get the etid and gid of this item.
    $parser = feeds_importer($this->id)->parser;
    $targets = array();
    foreach ($this->config['mappings'] as $mapping) {
      $targets[$mapping['target']] = $parser->getSourceElement($source, $result, $mapping['source']);
    }
    $targets = $this->convertTargets($targets);

    // If we're able to get both edit and gid, lookup any existing og_memberships.
    if (isset($targets['gid']) && isset($targets['etid'])) {
      $og_membership = og_get_membership($this->config['group_type'], $targets['gid'], $this->config['entity_type'], $targets['etid']);
      return $og_membership->id;
    }
  }
}
